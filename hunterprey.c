#include "hunterprey.h"
#include <math.h>
#include <time.h>

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("Please provide the environment file path.");
		exit(EXIT_FAILURE);
	}

	srand(time(NULL));
	environment env;

	int result = set_env(argv[1], &env);
	if (!result) {
		printf("Unable to set environments from given file.");
		exit(EXIT_FAILURE);
	}

	print_env(env);
	start(&env);

	clear_env(&env);
	exit(EXIT_SUCCESS);
}

int set_env(char *envFilePath, environment *env) {
	if (env == NULL) {
		return 0;
	}

	FILE * fp = fopen(envFilePath, "r");
	if (fp == NULL) {
		printf("Unable to open environment file: %s", envFilePath);
		return 0;
	}

	fscanf(fp, "%zd ", &(env->n));
	fscanf(fp, "%zd %zd ", &(env->d), &(env->e));
	fscanf(fp, "%zd ", &(env->R));
	fscanf(fp, "%lf ", &(env->T));

	size_t n = env->n;
	env->tick = 0;
	env->clear_env_per_tick = n * n / 2;

	env->grid = malloc(n*sizeof(grid_element *));

	// Initialize the grid
	size_t i = 0, j = 0;
	for (i = 0; i < n; i++) {
		env->grid[i] = malloc(n*sizeof(grid_element));
		for (j = 0; j < n; j++) {
			vec2 index = (vec2) { .x = i, .y = j };
			env->grid[i][j] = (grid_element) { .type = EMPTY, .index = index };
		}
	}

	// Fill the grid with given environment object
	char c;
	size_t x;
	size_t y;
	while (!feof(fp)) {
		if (fscanf(fp, "%c %zd %zd ", &c, &x, &y) != 3) {
			printf("Unable to set grid value. c: %c x: %zd y: %zd\n", c, x, y);
			break;
		}
		grid_type type = get_grid_type(c);
		size_t e = type == HUNTER ? env->e : type == PREY ? env->R : 0;
		vec2 index = (vec2) { .x = x - 1, .y = y - 1 };
		env->grid[(int)index.x][(int)index.y] = (grid_element) {
			.type = type, .energy = e, .index = index
		};
	}

	fclose(fp);
	return 1;
}

int start(environment *env) {
	char arg = '\0';
	while (arg != 'q') {
		scanf("%c", &arg);
		if (arg == '\n') {
			movement *movements;
			clear_marks(env);
			if (!update_agent_decisions(*env, &movements)) {
				printf("Unable to update agent decisions.\n");
				continue;
			}
			if (!feed_hunters(env)) {
				printf("Unable to feed hunters.\n");
				continue;
			}
			if (!apply_movements(env, movements)) {
				printf("Unable to apply agent movements.\n");
				continue;
			}
			if (movements)
			{
				free(movements);
			}
			env->tick += 1;
			print_env(*env);
		}
	}
	return 0;
}

void clear_marks(environment* env)
{
	if (env->tick % env->clear_env_per_tick != 0)
	{
		return;
	}
	size_t i, j;
	for (i = 0; i < env->n; i++) {
		for (j = 0; j < env->n; j++) {
			grid_element el = env->grid[i][j];
			el.hunter_mark = 0;
			env->grid[i][j] = el;
		}
	}
}

int update_agent_decisions(environment env, movement **movements) {
	size_t i, j;
	*movements = NULL;

	for (i = 0; i < env.n; i++) {
		for (j = 0; j < env.n; j++) {
			grid_element el = env.grid[i][j];
			if (el.type == EMPTY || el.type == OBSTACLE) {
				continue;
			}

			vec2 pos = { i, j };
			movement move;

			if (el.type == PREY) {
				move = update_prey_decision(env, pos);
			}
			else if (el.type == HUNTER) {
				move = update_hunter_decision(env, pos);
			}

			move.next = *movements;
			*movements = malloc(sizeof(movement));
			**movements = move;
		}
	}
	return 1;
}

int feed_hunters(environment *env) {
	if (!env) {
		return 0;
	}

	int i, j;
	for (i = 0; i < env->n; i++) {
		for (j = 0; j < env->n; j++) {
			grid_element el = env->grid[i][j];
			if (el.type == PREY) {
				size_t hunter_size;
				grid_element *neighbors;
				get_neighbors(*env, i, j, HUNTER, &neighbors, &hunter_size);
				if (hunter_size == 0) {
					// There is no hunter neighbor for this prey
					continue;
				}
				size_t energy = el.energy / hunter_size;
				int ni;
				for (ni = 0; ni < hunter_size; ni++) {
					// For each hunter neighbors
					grid_element hunter = neighbors[ni];
					hunter.energy += energy;
					hunter.is_stationary = 1;
					env->grid[(int)hunter.index.x][(int)hunter.index.y] = hunter;
				}
				// Kill the prey
				env->grid[i][j] = (grid_element) { .type = EMPTY, .index = el.index };

				if (neighbors) {
					free(neighbors);
				}
			}
		}
	}
	return 1;
}

int apply_movements(environment *env, movement* movements) {
	if (!env->grid) {
		return 0;
	}

	movement *move = movements;
	while (move) {
		size_t from_i = move->from.x, from_j = move->from.y;
		size_t to_i = move->to.x, to_j = move->to.y;

		grid_element el = env->grid[from_i][from_j];
		grid_element to_el = env->grid[to_i][to_j];

		vec2 from_index = el.index;
		grid_element from_el = (grid_element) { .type = EMPTY, .energy = 0, .index = from_index };

		if (el.is_stationary) {
			// Skip stationary agents (i.e fed hunters)
			el.is_stationary = 0;
			env->grid[from_i][from_j] = el;
			move = move->next;
			continue;
		}

		if (to_el.type == OBSTACLE || el.type == to_el.type) {
			// Skip obstacles and collisions
			move = move->next;
			continue;
		}

		if (el.type == HUNTER) {
			// Decrease the energy of hunters on each movement
			el.energy -= 1;
			if (el.energy < env->T) {
				// Kill the hunter
				env->grid[from_i][from_j] = from_el;
				move = move->next;
				continue;
			}
			// Left a mark where hunter leaves
			from_el.hunter_mark = 1;
		}

		el.index = (vec2) { .x = to_i, .y = to_j };
		env->grid[from_i][from_j] = from_el;
		env->grid[to_i][to_j] = el;
		move = move->next;
	}
	return 1;
}

movement update_prey_decision(environment env, vec2 pos) {
	vec2 delta = { 0, 0 };
	size_t i, j;
	for (i = 0; i < env.n; i++) {
		for (j = 0; j < env.n; j++) {
			if (env.grid[i][j].type == HUNTER) {
				// Perceive
				// Calculate vectors between this prey and the hunters
				vec2 hunter_pos = { i, j };
				vec2 sub = vec_sub(pos, hunter_pos);
				delta = vec_add(delta, vec_mul(sub, 1 / vec_mag(sub)));
			}
		}
	}

	// Decision
	// Add delta to current position to find final position
	delta = vec_clamp(-1, 1, delta);
	delta = vec_round(delta);
	vec2 to_pos = vec_add(pos, delta);

	// Limit the movement in grid borders
	to_pos = vec_clamp(0, env.n - 1, to_pos);
	movement move = (movement) { .from = pos, .to = to_pos };
	return move;
}

movement update_hunter_decision(environment env, vec2 pos) {
	grid_element el = env.grid[(int)pos.x][(int)pos.y];
	size_t i, j;
	size_t prey_found = 0;
	vec2 nearest_prey_pos = { 0 };
	double min_prey_distance = env.n * sqrt(2);

	for (i = 0; i < env.n; i++) {
		for (j = 0; j < env.n; j++) {
			if (env.grid[i][j].type == PREY) {
				// Perceive
				vec2 prey_pos = { i, j };
				double distance = vec_mag(vec_sub(prey_pos, pos));
				if (distance > env.d) {
					// Hunter can not see this prey
					continue;
				}
				if (distance < min_prey_distance) {
					min_prey_distance = distance;
					nearest_prey_pos = prey_pos;
					prey_found = 1;
				}
			}
		}
	}

	// Decision
	vec2 to_pos = get_next_hunter_pos(env, pos, el, prey_found, nearest_prey_pos);

	// Limit the movement in grid borders
	to_pos = vec_clamp(0, env.n - 1, to_pos);
	movement move = (movement) { .from = pos, .to = to_pos };
	return move;
}

vec2 get_next_hunter_pos(environment env, vec2 pos, grid_element el, size_t prey_found, vec2 nearest_prey_pos)
{
	vec2 delta = { 0 };
	if (prey_found) {
		delta = vec_norm(vec_sub(nearest_prey_pos, pos));
	}
	else {
		// Prey not found. If the hunter has enough energy,
		// move randomly to search for new preys.
		if (el.energy <= env.T * 2) {
			// Preserve energy
			delta = (vec2) { .x = 0, .y = 0 };
		}
		else {
			// Random movement
			size_t neighbor_size;
			grid_element *neighbors;
			get_neighbors(env, (int)pos.x, (int)pos.y, EMPTY, &neighbors, &neighbor_size);
			if (neighbor_size > 0) {
				int ni;
				// Check for each neighbors and try to move one which does
				// not have a hunter mark
				for (ni = 0; ni < neighbor_size; ni++)
				{
					grid_element n = neighbors[ni];
					if (!n.hunter_mark)
					{
						return n.index;
					}
				}
			}
			// If there is no neighbor available, select a random one
			delta = (vec2) { .x = rand() % 3 + -1, .y = rand() % 3 + -1 };
		}
	}
	delta = vec_clamp(-1, 1, delta);
	delta = vec_round(delta);
	vec2 to_pos = vec_add(pos, delta);
	return to_pos;
}