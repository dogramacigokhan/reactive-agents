#### Reactive Agents

Simple hunter prey project to simulate reactive behaviors of multiple agents.

#### Building the Source Code
* Run “make” in source directory, executable “prog” file should be created.
* Run executable “prog” file:
	./prog input_file
* input_file: [Required] Input file which contains environment information for initialization
#### Source code is tested on:
* Ubuntu 16.04 LTS, GCC 4.8.4, c99 Standard,
* Windows 10, Visual Studio 2015, Windows C/C++ Compiler