/* Quackmatic 2014
 * https://github.com/Quackmatic
 */

#include <math.h>
#include "vector2.h"
#include "util.h"

vec2 vec_add(vec2 v1, vec2 v2)
{
    vec2 value = { v1.x + v2.x, v1.y + v2.y };
    return value;
}

vec2 vec_sub(vec2 v1, vec2 v2)
{
    vec2 value = { v1.x - v2.x, v1.y - v2.y };
    return value;
}

vec2 vec_mul(vec2 v, real d)
{
    vec2 value = { v.x * d, v.y * d };
    return value;
}

vec2 vec_div(vec2 v, real d) {
	vec2 value = { d / v.x, d / v.y };
	return value;
}

real vec_mag(vec2 v)
{
    return sqrt(v.x * v.x + v.y * v.y);
}

vec2 vec_norm(vec2 v)
{
    return vec_mul(v, 1 / vec_mag(v));
}

real vec_dot(vec2 v1, vec2 v2)
{
    return v1.x * v2.x + v1.y * v2.y;
}

real vec_dist(vec2 v1, vec2 v2)
{
    return vec_mag(vec_sub(v1, v2));
}

vec2 vec_clamp(real min, real max, vec2 v)
{
	vec2 value = { clamp(min, max, v.x), clamp(min, max, v.y) };
	return value;
}

vec2 vec_round(vec2 v)
{
	vec2 value = { round(v.x), round(v.y) };
	return value;
}