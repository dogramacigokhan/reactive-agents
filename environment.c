#include "environment.h"

grid_type get_grid_type(char c) {
	switch (c) {
	case 'h':
		return HUNTER;
	case 'p':
		return PREY;
	case 'o':
		return OBSTACLE;
	default:
		return EMPTY;
	}
}

char grid_type_to_char(grid_type type) {
	switch (type) {
	case HUNTER:
		return 'h';
	case PREY:
		return 'p';
	case OBSTACLE:
		return 'o';
	default:
		return '-';
	}
}

void get_neighbors(environment env, int i, int j, grid_type type,
	grid_element **neigbors, size_t *size) {
	*size = 0;
	*neigbors = malloc(sizeof(grid_element));

	int x, y;
	for (x = max(0, i - 1); x <= min(i + 1, env.n - 1); x++) {
		for (y = max(0, j - 1); y <= min(j + 1, env.n - 1); y++) {
			if (x != i || y != j) {
				grid_element el = env.grid[x][y];
				if (el.type == type) {
					// For each neighbors
					*neigbors = realloc(*neigbors, ((*size) + 1)*sizeof(grid_element));
					(*neigbors)[(*size)++] = el;
				}
			}
		}
	}
}

void print_env(environment env) {
	size_t i, j;
	for (i = 0; i < env.n; i++) {
		for (j = 0; j < env.n; j++) {
			printf("%c ", grid_type_to_char(env.grid[i][j].type));
		}
		printf("\n");
	}
	for (i = 0; i < env.n; i++) {
		for (j = 0; j < env.n; j++) {
			grid_element el = env.grid[i][j];
			if (el.type == HUNTER || el.type == PREY) {
				printf("%c: %zd ", grid_type_to_char(el.type), el.energy);
			}
		}
	}
	printf("| Press Enter to move agents\n");
}

void clear_env(environment *env)
{
	if (env->grid) {
		free(env->grid);
	}
}