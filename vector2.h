/* Quackmatic 2014
 * https://github.com/Quackmatic
 */

#ifndef VECTOR2_H
#define VECTOR2_H

#include "util.h"

typedef struct { real x; real y; } vec2;

vec2 vec_add(vec2 v1, vec2 v2);

vec2 vec_sub(vec2 v1, vec2 v2);

vec2 vec_mul(vec2 v, real d);

vec2 vec_div(vec2 v, real d);

real vec_mag(vec2 v);

vec2 vec_norm(vec2 v);

real vec_dot(vec2 v1, vec2 v2);

real vec_dist(vec2 v1, vec2 v2);

vec2 vec_clamp(real min, real max, vec2 v);

vec2 vec_round(vec2 v);

#endif