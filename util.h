#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>

#ifdef USE_FLOATS
    typedef float real; 
#else 
    typedef double real;
#endif

#define min(X,Y) ((X) < (Y) ? (X) : (Y))
#define max(X,Y) ((X) > (Y) ? (X) : (Y))

double clamp(double min, double max, double val);

#endif
