#ifndef HUNTERPREY_H
#define HUNTERPREY_H

#include <stdlib.h>
#include <stdio.h>
#include "environment.h"
#include "vector2.h"

int set_env(char *envFilePath, environment* env);
int start(environment *env);

void clear_marks(environment* env);
int update_agent_decisions(environment env, movement **movements);
int feed_hunters(environment *env);
int apply_movements(environment *env, movement *movements);

movement update_prey_decision(environment env, vec2 pos);
movement update_hunter_decision(environment env, vec2 pos);
vec2 get_next_hunter_pos(environment env, vec2 pos, grid_element el, size_t prey_found, vec2 nearest_prey_pos);

#endif
