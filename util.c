#include "util.h"

double clamp(double min, double max, double val) {
	if (val < min) {
		return min;
	}
	else if (val > max) {
		return max;
	}
	return val;
}
