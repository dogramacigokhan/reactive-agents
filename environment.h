#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <stdlib.h>
#include <stdio.h>
#include "util.h"
#include "vector2.h"

typedef enum grid_type {
	EMPTY,
	PREY,
	HUNTER,
	OBSTACLE
} grid_type;

typedef struct movement {
	vec2 from;
	vec2 to;
	struct movement *next;
} movement;

typedef struct grid_element {
	vec2 index;				// Index of the element in grid
	grid_type type;			// Type of the element
	size_t energy;			// Energy value
	size_t is_stationary;	// Flag to lock el. to current location for one turn
	size_t hunter_mark;		// Does this grid el. contains a hunter mark?
} grid_element;

typedef struct environment {
	size_t n;	// Grid size nxn
	size_t d;	// Hunter vision
	size_t e;	// Initial energy for hunters
	size_t R;	// Enery bonus for prey
	real T;		// Minimum energy threshold for hunters
	grid_element **grid;

	// Tick count of the environment
	size_t tick;
	// Hunter marks will be cleaned per this tick count
	size_t clear_env_per_tick;
} environment;

grid_type get_grid_type(char c);
char grid_type_to_char(grid_type type);
void get_neighbors(environment env, int i, int j, grid_type type,
	grid_element **neigbors, size_t *size);

void print_env(environment env);
void clear_env(environment *env);

#endif
